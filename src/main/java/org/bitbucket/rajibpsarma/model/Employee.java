package org.bitbucket.rajibpsarma.model;

/*
 * @author Rajib Sarma
 */
public class Employee {
	private String id;
	private String name;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Employee(String id, String name) {
		super();
		this.id = id;
		this.name = name;
	}
	public Employee() {}
	public String toString( ) {
		return "Id : \"" + id + "\", Name : \"" + name + "\"";
	}
	public boolean equals(Employee e1, Employee e2) {
		return (e1.getName().equalsIgnoreCase(e2.getName()));
	}
}