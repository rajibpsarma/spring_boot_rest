package org.bitbucket.rajibpsarma.rest;

import java.util.List;

import org.bitbucket.rajibpsarma.model.Employee;
import org.bitbucket.rajibpsarma.service.EmployeeService;
import org.bitbucket.rajibpsarma.service.EmployeeService.SORT_ON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest")
public class MyRestController {

	
	@Autowired
	private EmployeeService empService;
	
	
	// Add employee
	@PostMapping("/employees")
	public ResponseEntity<String> addEmployee(@ModelAttribute Employee emp) {
		ResponseEntity<String> response = null;
		try {
			empService.addEmployee(emp);
			response = new ResponseEntity<String>(HttpStatus.CREATED);
		}
		catch(Exception ex) {
			String msg = "Error while creating employee with details " + emp;
			response = new ResponseEntity<String>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	
	// get all employees
	@GetMapping("/employees")
	public ResponseEntity<Object> getAllEmployees() {
		ResponseEntity response = null;
		try {
			List<Employee> list = empService.getAllEmployees();
			response = new ResponseEntity<Object>(list, HttpStatus.OK);
		} catch(Exception ex) {
			String msg = "Error while fetching all employee details.";
			response = new ResponseEntity<String>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	
	// get all employees sorted on id/name
	@GetMapping("/employeesSorted")
	public ResponseEntity getAllSortedEmployees(@RequestParam String sortOn, Model model) {
		SORT_ON sort_on = SORT_ON.build(sortOn);
		
		// If proper value is not passed in sortOn, simply display all the employees
		if(sort_on == null) {
			return getAllEmployees();
		}
		
		ResponseEntity response = null;
		
		// Need to sort
		try {
			List<Employee> list = empService.getAllSortedEmployees(sort_on);
			response = new ResponseEntity<Object>(list, HttpStatus.OK);
		} catch(Exception ex) {
			String msg = "Error while fetching all the employees with us, sorted on \"" + sortOn + "\"";
			response = new ResponseEntity<String>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		return response;
	}
	
	
	// get an employee by id
	@GetMapping(value = "/employees/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Object> getEmployee(@PathVariable String id, Model model) {
		ResponseEntity response = null;
		
		try {
			Employee emp = empService.getEmployeeById(id);
			if(emp != null) {
				response = new ResponseEntity<Object>(emp, HttpStatus.OK);
			}
			else {
				// Not found, 404
				String msg = "The Employee with id '" + id + "' is not found.";
				response = new ResponseEntity<String>(msg, HttpStatus.NOT_FOUND);
				//response = ResponseEntity.status(HttpStatus.NOT_FOUND).build();
			}
		} catch(Exception ex) {
			String msg = "Error while fetching the details of the employee with id \"" + id + "\"";
			response = new ResponseEntity<String>(msg, HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return response;
	}
}
