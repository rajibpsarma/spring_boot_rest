# REST controller using Spring Boot #

It's a demo app that allows to add employees, get all employees, get all employees sorted on their id / name and get an employee based on Id.

## The steps to be followed are: ##
* git clone https://rajibpsarma@bitbucket.org/rajibpsarma/spring_boot_rest.git
* cd spring_boot_rest
* mvn clean spring-boot:run
* Now explore the app using "http://localhost:8080/"